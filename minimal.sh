#!/usr/bin/env bash
set -euxo pipefail

# You'll want ctags for Markdown, C++, and Python apps
apt-get install --yes --no-install-recommends \
    ca-certificates \
    git \
    shellcheck \
    universal-ctags \
    vim \
    wget \
    yamllint

# hadolint is a linter detected by ALE
#
# Install binaries to /usr/local/bin:
# https://askubuntu.com/a/6903/612216
wget https://github.com/hadolint/hadolint/releases/download/v1.18.0/hadolint-Linux-x86_64 \
 && chmod +x hadolint-Linux-x86_64 \
 && mv hadolint-Linux-x86_64 /usr/local/bin/hadolint
