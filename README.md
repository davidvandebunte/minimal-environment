# Minimal environment

An environment with my personal most commonly used tools.

For tips on how to develop this environment on a host, see `.tmuxinator.yml`.
