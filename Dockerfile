FROM ubuntu:focal-20200423 as builder

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

COPY minimal.sh .
RUN apt-get update && ./minimal.sh \
    && rm -rf /var/lib/apt/lists/*

USER $UNAME
WORKDIR $HOME

FROM builder as runtime
ARG UNAME="vandebun"
RUN test -n "$UNAME"
ARG USER_ID="1000"
RUN test -n "$USER_ID"
ARG USER_GID="1000"
RUN test -n "$USER_GID"
RUN groupadd \
    --gid "$USER_GID" \
    --non-unique "$UNAME"
RUN useradd --create-home \
    --uid "$USER_ID" \
    --gid "$USER_GID" \
    --shell /bin/bash \
    "$UNAME"
ENV USER=$UNAME
ENV HOME=/home/$UNAME
WORKDIR $HOME
USER $UNAME

RUN git clone https://gitlab.com/davidvandebunte/dotfiles.git \
 && git -C dotfiles checkout 476aa19 \
 && ./dotfiles/setup-dotfiles.sh
